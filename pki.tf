module "intermediate_ca" {
  source = "/home/acohea/Code/gitlab.com/adriennes-spells/vault_certificate_authority"

  address      = local.vault.signatures.endpoints[0]
  token        = local.vault.signatures.root_token
  ca_cert_file = pathexpand(var.root_ca_certificate_file)

  path         = "intermediate"
  description  = "Intermediate Certificate Authority"
  common_name  = "Valkyrie Infrastructure Research - Intermediate Certificate Authority"
  organization = "Valkyrie Infrastructure Research"

  intermediate_ca_key_file  = var.intermediate_ca_private_key_file
  intermediate_ca_cert_file = var.intermediate_ca_certificate_file
  root_ca_file              = pathexpand(var.root_ca_certificate_file)
}

module "consul_ca" {
  source = "/home/acohea/Code/gitlab.com/adriennes-spells/vault_certificate_authority"

  address      = local.vault.signatures.endpoints[0]
  token        = local.vault.signatures.root_token
  ca_cert_file = pathexpand(var.root_ca_certificate_file)

  path              = "consul-authoritative-datacenter"
  intermediate_path = module.intermediate_ca.path
  description       = "Consul Authoritative Datacenter"
  common_name       = "Consul Authoritative Datacenter - Intermediate CA"
  organization      = "Valkyrie Infrastructure Research"
}
