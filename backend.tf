terraform {
  backend "gcs" {
    bucket = "adrienne-devops-terraform-states"
    prefix = "gitlab.com/valkyrie-polite-society/asgard/authorities"
  }
}
