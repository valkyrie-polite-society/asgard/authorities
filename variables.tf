variable "intermediate_ca_private_key_file" {
  type = string
}

variable "intermediate_ca_certificate_file" {
  type = string
}

variable "root_ca_certificate_file" {
  type = string
}
