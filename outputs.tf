output "consul" {
  value = module.consul_ca
}

output "internal_endpoints" {
  value = local.vault.signatures.internal_endpoints
}

output "endpoints" {
  value = local.vault.signatures.endpoints
}

output "address" {
  value = "https://${local.vault.signatures.fqdn}:8200"
}
