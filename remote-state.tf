data "terraform_remote_state" "vault" {
  backend = "gcs"

  config = {
    bucket = "adrienne-devops-terraform-states"
    prefix = "gitlab.com/valkyrie-polite-society/asgard/vault"
  }
}

locals {
  vault = data.terraform_remote_state.vault.outputs
}
